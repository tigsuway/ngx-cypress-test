/// <reference types="cypress" />;

describe("Our first test suite", () => {
  it("first test", () => {
    cy.visit("/");
    cy.contains("Forms").click();
    cy.contains("Form Layouts").click();

    // tag
    cy.get("input");

    //id
    cy.get("#inputEmail1");

    // class
    cy.get(".input-full-width");

    // attribute
    cy.get("[placeholder]");

    // attribute and value
    cy.get('[placeholder="Email"]');

    //class value
    cy.get('[class="input-full-width size-medium shape-rectangle"]');

    // tag name and attribute value
    cy.get('input[placeholder="Email"]');

    // two different attributes
    cy.get('input[placeholder="Email"][type="email"]');

    //by tag name, attribute value, ID, class name
    cy.get('input[placeholder="Email"]#inputEmail1.input-full-width');

    // most recommended way by Cypress
    cy.get('[data-cy="inputEmail1"]');
  });

  // "only" at "it.only" will execute this test case only.
  it.only("second test", () => {
    cy.visit("/");
    cy.contains("Forms").click();
    cy.contains("Form Layouts").click();

    cy.get('[data-cy="signInButton"]');
    cy.contains("Sign in");
    cy.contains('[status="warning"]', "Sign in");
    cy.get("#inputEmail3")
      .parents("form")
      .find("button")
      .should("contain", "Sign in")
      .parents("form")
      .find("nb-checkbox");
    cy.contains("nb-card", "Horizontal form").find('[type="email"]');
  });
});
