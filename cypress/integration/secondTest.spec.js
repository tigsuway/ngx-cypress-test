/// <reference types="Cypress" />;

// const { values } = require("cypress/types/lodash");

describe("Our first test suite", () => {
  it("then and wrap methods", () => {
    cy.visit("/");
    cy.contains("Forms").click();
    cy.contains("Form Layouts").click();

    cy.contains("nb-card", "Using the Grid")
      .find('[for="inputEmail1"]')
      .should("contain", "Email");

    cy.contains("nb-card", "Using the Grid")
      .find('[for="inputPassword2"]')
      .should("contain", "Password");

    cy.contains("nb-card", "Basic form")
      .find('[for="exampleInputEmail1"]')
      .should("contain", "Email");

    cy.contains("nb-card", "Basic form")
      .find('[for="exampleInputPassword1"]')
      .should("contain", "Password");

    // Selenium Style
    // creating variables to assign repetitive commands

    // const firstForm = cy.contains("nb-card", "Using the Grid");
    // const secondForm = cy.contains("nb-card", "Basic form");

    // // Now user the const variable above to the chain commands
    // firstForm.find('[for="inputEmail1"]').should("contain", "Email");
    // secondForm.find('[for="exampleInputEmail1"]').should("contain", "Email");

    //  Above doesn't work because cypress is asynchronous, it "won't" save the result of the firstform to a variable and then use the result to run the following chain of commands.

    // Cypress style - Saving Subject of Command
    cy.contains("nb-card", "Using the Grid").then((firstForm) => {
      // .find below is a Jquery format
      const emailLabelFirst = firstForm.find('[for="inputEmail1"]').text();
      const passwordLabelFirst = firstForm
        .find('[for="inputPassword2"]')
        .text();

      // When making assertions to Jquery library you need to use expect which is a Chai library but if cypress library use should
      expect(emailLabelFirst).to.equal("Email");
      expect(passwordLabelFirst).to.equal("Password");
    });

    // Compare if the Password label of 2 forms are equal.
    cy.contains("nb-card", "Using the Grid").then((firstForm) => {
      const firstFormPasswordLabel = firstForm
        .find('[for="inputPassword2"]')
        .text();

      cy.contains("nb-card", "Basic form").then((secondForm) => {
        const secondFormPasswordLabel = secondForm
          .find('[for="exampleInputPassword1"]') // .find here is from jquery library
          .text();
        expect(firstFormPasswordLabel).to.equal(secondFormPasswordLabel);

        // Switching back to using Cypress Library
        cy.wrap(secondForm)
          .find('[for="exampleInputPassword1"]') // .find here is now from cypress library
          .should("contain", "Password");
      });
    });
  });
  it("invoke command", () => {
    cy.visit("/");
    cy.contains("Forms").click();
    cy.contains("Form Layouts").click();

    // Example #1:
    cy.get('[for="exampleInputEmail1"]').should("contain", "Email address");

    // Example #2:
    cy.get('[for="exampleInputEmail1"]').then((label) => {
      expect(label.text()).to.equal("Email address");
    });

    // Example #3:
    cy.get('[for="exampleInputEmail1"]')
      //  Invoke functions to get text from our webpage
      .invoke("text")
      .then((text) => {
        expect(text).to.equal("Email address");
      });

    //  Example #4:
    cy.contains("nb-card", "Basic form")
      .find("nb-checkbox")
      .click()
      .find(".custom-checkbox")
      // Get different attribute in the element
      .invoke("attr", "class")
      // .should("contain", "checked"); // Cypress assertion
      // using Chai assertion
      .then((classValue) => {
        expect(classValue).to.contain("checked");
      });
  });

  it.only("assert property", () => {
    cy.visit("/");
    cy.contains("Forms").click();
    cy.contains("Datepicker").click();
    cy.contains("nb-card", "Common Datepicker")
      .find("input")
      .then((input) => {
        cy.wrap(input).click();
        cy.get("nb-calendar-day-picker").contains("17").click();
        cy.wrap(input).invoke("prop", "value").should("equal", "Mar 17, 2021");
      });

    // .then((input) => {
    //   cy.wrap(input).click();
    //   cy.get();
    // });
  });
});
